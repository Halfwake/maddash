import pyglet

class Tweenable(object):
    def interpolate(self, a, b, step_amount, style = "linear"):
        if style == "linear":
            steps = []
            step_distance = abs(a - b) / float(step_amount)
            low = min((a, b))
            high = max((a, b))
            reverse_flag = low == b
            for i in xrange(step_amount):
                low += step_distance
                steps.append(low)
            final_list = map(lambda a : int(round(a)), steps)
            if reverse_flag: final_list.reverse()
            return final_list
        elif style == "acceleration":
            steps = []
            for t in range(step_amount):
                steps.append(a + t**2 * (b - a))
            return steps
        elif style == "deceleration":
            steps = []
            for t in range(step_amount):
                steps.append(a + (1.0 - (1.0 - t)**2) * (b - a))
            return steps
    def tween(self, x, y, time_limit, steps, style = "linear"):
        time = [0.0]
        change_x = self.interpolate(self.x, x, steps, style)
        change_y = self.interpolate(self.y, y, steps, style)
        
        block_x, block_y = False, False
        if self.x == x: block_x = True
        if self.y == y: block_y = True
        
        def _lambda(dt):
            time[0] += dt
            if time[0] >= time_limit:
                if not block_x: self.x = change_x[-1]
                if not block_y: self.y = change_y[-1]
                pyglet.clock.unschedule(_lambda)
            else:
                if not block_x: self.x = change_x[int(round((len(change_x) - 1) * (time[0] / time_limit)))]
                if not block_y: self.y = change_y[int(round((len(change_y) - 1) * (time[0] / time_limit)))]
        pyglet.clock.schedule(_lambda)
        return _lambda
