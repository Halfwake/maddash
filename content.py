import pyglet
from os import sep

CONTENT_PATH = "content"
IMAGE_PATH = CONTENT_PATH + sep + "image"
SOUND_PATH = CONTENT_PATH + sep + "sound"

def image_load(file_name):
    return pyglet.image.load(IMAGE_PATH + sep + file_name)

def animation_load(file_name):
    animation = pyglet.image.load_animation(IMAGE_PATH + sep + file_name)
    return animation

def sound_load(file_name):
    return pyglet.media.load(SOUND_PATH + sep + file_name)

IMAGE = {"emi_running" : animation_load("emi_running.gif"),
         "background1" : image_load("background1.png"),
         "hurdle1" : image_load("hurdle1.png"),
         "life1" : image_load("life1.png"),
         "life2" : image_load("life2.png"),
         "startbutton1" : image_load("startbutton1.png"),
         "quitbutton1" : image_load("quitbutton1.png"),
         "title1" : image_load("title1.png"),
         "thought1" : image_load("thought1.png"),
         "thought2" : image_load("thought2.png"),
         "thought3" : image_load("thought3.png"),
         "retrybutton1" : image_load("retrybutton1.png"),
         "gameover1" : image_load("gameover1.png"),
         "rin1" : image_load("rin1.png")}

##SOUND = {"focus" : sound_load("focus.ogg"),
##         "footsteps1" : sound_load("footsteps1.mp3")}
