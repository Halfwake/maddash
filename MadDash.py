#!/usr/bin/env python
import pyglet
import random

from content import IMAGE#, SOUND
from tween import Tweenable

SCREEN_WIDTH, SCREEN_HEIGHT = 1000, 400
TITLE = "Emi's Mad Dash"

ACTION_KEY = pyglet.window.key.SPACE
PAUSE_KEY = pyglet.window.key.P
KEY_PRESS_TIME = 0.50
START_LIVES = 3
FONT_SIZE = 24

class Input(object):
    key_down = set()
    key_press = set()
    mouse_down = set()
    mouse_x, mouse_y = 0, 0
    @staticmethod
    def KeyDown(key):
        return (key in Input.key_down)
    @staticmethod
    def MouseDown(button):
        if (button in Input.mouse_down):
            return (Input.mouse_x, Input.mouse_y)
    @staticmethod
    def KeyPress(key):
        return (key in Input.key_press)
    @staticmethod
    def RemovePress(key):
        if key in Input.key_press:
            Input.key_press.remove(key)
    
class Collide(object):
    def intersect(self, other):
        collide = False
        if self.x <= other.x <= self.x + self.width:
            if self.y <= other.y <= self.y + self.height:
                collide = True
        if other.x <= self.x <= other.x + other.width:
            if other.y <= self.y <= other.y + other.height:
                collide = True
        return collide
    
class Image(pyglet.sprite.Sprite):
    def __init__(self, start_image):
        self.start_image = start_image
        super(Image, self).__init__(IMAGE[self.start_image])
    def set_image(self, image_name):
        if self.image_name != image_name:
            self.image = content.IMAGE[image_name]
            self.image_name = image_name

class Button(Image, Collide):
    def __init__(self, owner, x, y, image_name, on_press):
        self.owner = owner
        super(Button, self).__init__(image_name)
        self.x = x
        self.y = y
        self.on_press = on_press
        

class Hurdle(Image, Tweenable, Collide):
    def __init__(self, owner, x, y):
        self.owner = owner
        super(Hurdle, self).__init__("hurdle1")
        self.x = x
        self.y = y
        self.move_time = 0.25
        self.move_distance = 100
        self.moving = False
        self.passed = True
        self.value = 50
    def on_update(self, dt):
        if not self.moving:
            self.moving = True
            def _lambda(dt):
                self.moving = False
            self.tween(self.x - 100, self.y, self.move_time, 50)
            pyglet.clock.schedule_once(_lambda, self.move_time)
        if self.x + self.width < 0:
            if self.passed:
                self.owner.add_points(self.value)
                self.owner.increase_multiplier()
            self.owner.hurdles.pop(self.owner.hurdles.index(self))
            
class Thought(Image, Tweenable):
    Thoughts = ("thought2", "thought3",)
    def __init__(self, owner, x, y):
        self.owner = owner
        super(Thought, self).__init__(random.choice(Thought.Thoughts))
        self.x = x
        self.y = y
        
class Rin(Hurdle):
    def __init__(self, owner, x, y):
        super(Rin, self).__init__(owner, x, y)
        self.image = IMAGE["rin1"]
        self.thought_x_offset = 25
        self.thought_y_offset = 50
        self.make_thought()
    def make_thought(self):
        x = self.x + self.thought_x_offset
        y = self.y + self.thought_y_offset
        self.thought = Thought(self, x, y)
    def on_update(self, dt):
        if not self.moving:
            self.moving = True
            def _lambda(dt):
                self.moving = False
            self.tween(self.x - 100, self.y, self.move_time, 50)
            self.thought.tween(self.thought.x - 100, self.thought.y, self.move_time, 50)
            pyglet.clock.schedule_once(_lambda, self.move_time)
        if self.thought.x + self.thought.width < 0:
            if self.passed:
                self.owner.add_points(self.value)
                self.owner.increase_multiplier()
            print 1
            self.owner.rin_present = False
            self.owner.hurdles.pop(self.owner.hurdles.index(self))
    def draw(self):
        super(Rin, self).draw()
        self.thought.draw()
        
class Player(Image, Tweenable, Collide):
    def __init__(self, owner, x, y):
        self.owner = owner
        super(Player, self).__init__("emi_running")
        self.x = x
        self.y = y
        self.stunned = False
        self.stun_draw_flag = False
        self.stun_length = 1.00
        self.jumping = False
        self.falling = False
        self.jump_height = 100
        self.up_time = 0.3
        self.down_time = 0.5
    def draw(self):
        _super = lambda : super(Player, self).draw()
        if not self.stunned:
            _super()
        elif self.stunned:
            if self.stun_draw_flag:
                _super()
            self.stun_draw_flag = not self.stun_draw_flag
    def on_update(self, dt):
        if self.owner.playing == True:
            if Input.KeyDown(ACTION_KEY):
                self.jump()
        self.collide()
    def fall(self):
        if self.falling:
            def _lambda(dt):
                self.falling = False
            self.tween(self.x, self.owner.ground_y, self.down_time, 50)
            pyglet.clock.schedule_once(_lambda, self.down_time)
    def collide(self):
        if not self.stunned:
            for hurdle in self.owner.hurdles:
                if self.intersect(hurdle):
                    hurdle.passed = False
                    self.stumble()
    def stumble(self):
        self.stunned = True
        self.owner.reset_multiplier()
        def _lambda(dt):
            self.stunned = False
        self.owner.lose_life()
        pyglet.clock.schedule_once(_lambda, self.stun_length)
    def jump(self):
        if not self.jumping and not self.falling:
            self.jumping = True
            def _lambda(dt):
                self.jumping = False
                self.falling = True
                self.fall()
            self.tween(self.x, self.y +  self.jump_height, self.up_time, 50)
            pyglet.clock.schedule_once(_lambda, self.up_time)

class LifeContainer(object):
    def __init__(self, owner, x, y):
        self.owner = owner
        self.lives = self.owner.lives
        self.x = x
        self.y = y
        self.life_marker_distance_x = 50
        self.life_markers = []
        self.create_sprites()
    def reset_lives(self):
        self.life_markers = []
        self.lives = self.owner.lives
        self.create_sprites()
    def create_sprites(self):
        for i in range(self.lives):
            self.life_markers.append(pyglet.sprite.Sprite(IMAGE["life2"], self.x + i * self.life_marker_distance_x, self.y))
    def draw(self):
        for life_marker in self.life_markers: life_marker.draw()

class Mode(object):
    def __init__(self, owner):
        pass
    def on_draw(self):
        pass
    def on_update(self, dt):
        pass

class MenuMode(Mode):
    def __init__(self, owner):
        self.owner = owner
        self.background = pyglet.sprite.Sprite(IMAGE["title1"])
        self.button_x = 110
        self.button_y_offset = 155
        self.button_spacing_y = 50
        self.buttons = []
        self.make_buttons()
    def make_buttons(self):
        def _lambda():
            self.owner.set_mode_game()
        self.buttons.append(Button(self,
                                   self.button_x,
                                   SCREEN_HEIGHT - self.button_y_offset - (self.button_spacing_y * 0),
                                   "startbutton1",
                                   _lambda))
        self.buttons.append(Button(self,
                                   self.button_x,
                                   SCREEN_HEIGHT - self.button_y_offset - (self.button_spacing_y * 1),
                                   "quitbutton1",
                                   lambda : (pyglet.app.exit(), quit())))
                                    
    def on_draw(self):
        self.background.draw()
        for button in self.buttons: button.draw()
    def on_update(self, dt):
        mouse_pos = Input.MouseDown(pyglet.window.mouse.LEFT)
        if mouse_pos:
            x, y = mouse_pos[0], mouse_pos[1]
            for button in self.buttons:
                if button.x < x < button.x + button.width:
                    if button.y < y < button.y + button.height:
                        button.on_press()

class GameMode(Mode):
    def __init__(self, owner):
        self.owner = owner
        self.rin_present = False
        self.pause = False
        self.points = 0
        self.point_multiplier = 1
        self.lives = START_LIVES
        self.life_container = LifeContainer(self, 0, SCREEN_HEIGHT - 30)
        self.min_obstacle_time = 1
        self.max_obstacle_time = 2
        self.new_obstacle_time()
        self.obstacle_x = SCREEN_WIDTH + 100
        self.ground_y = 52
        self.emi_start_x = -75
        self.emi_end_x = 150
        self.emi_entrance_time = 1.5
        self.player = Player(self, self.emi_start_x, self.ground_y)
        self.hurdles = []
        self.background = pyglet.sprite.Sprite(IMAGE["background1"])
        self.make_thought_bubble()
        self.start_pausing = True
        self.start_message = pyglet.text.Label("Press SPACE to start! (P to pause)",
                                               bold = True,
                                               font_size = FONT_SIZE,
                                               x = SCREEN_WIDTH / 2 - 280,
                                               y = SCREEN_HEIGHT / 2)
        self.point_hud = pyglet.text.Label("%d" % self.points,
                                           bold = True,
                                           font_size = FONT_SIZE,
                                           x = 0,
                                           y = 25)
        self.multiplier_hud = pyglet.text.Label("x%d" % self.point_multiplier,
                                                bold = True,
                                                font_size = FONT_SIZE,
                                                x = 0,
                                                y = 0)
        self.pause_message = pyglet.text.Label("Press P to unpause",
                                               bold = True,
                                               font_size = FONT_SIZE,
                                               x = SCREEN_WIDTH / 2 - 150,
                                               y = SCREEN_HEIGHT / 2)
        self.starting = False
        self.playing = False
    def make_thought_bubble(self):
        self.thought_bubble = pyglet.sprite.Sprite(IMAGE["thought1"])
        self.thought_bubble.x = 200
        self.thought_bubble.y = 110
    def increase_multiplier(self):
        self.point_multiplier += 1
        self.multiplier_hud.text = "x%d" % self.point_multiplier
    def reset_multiplier(self):
        self.point_multiplier = 1
        self.multiplier_hud.text = "x%d" % self.point_multiplier
    def new_obstacle_time(self):
        self.cur_obstacle_time = random.randint(self.min_obstacle_time,
                                                self.max_obstacle_time)
    def on_draw(self):
        self.background.draw()
        self.point_hud.draw()
        self.multiplier_hud.draw()
        for hurdle in self.hurdles: hurdle.draw()
        self.player.draw()
        self.life_container.draw()
        if self.start_pausing:
            self.start_message.draw()
        if self.pause:
            self.pause_message.draw()
            self.thought_bubble.draw()
    def lose_life(self):
        self.lives -= 1
        if self.lives < 0:
            self.owner.set_mode_lose()
        self.life_container.reset_lives()
    def add_points(self, amount):
        self.points += amount * self.point_multiplier
        self.point_hud.text = "%d" % self.points
    def on_update(self, dt):
        if self.start_pausing:
            if Input.KeyDown(ACTION_KEY):
                self.start_pausing = False
                self.starting = True
        elif self.starting:
            def _lambda(dt):
                self.starting = False
                self.playing = True
            self.player.tween(self.emi_end_x,
                              self.player.y,
                              self.emi_entrance_time,
                              200)
            pyglet.clock.schedule_once(_lambda, self.emi_entrance_time)
        elif self.pause:
            if self.pause:
                if Input.KeyPress(PAUSE_KEY):
                    Input.RemovePress(PAUSE_KEY)
                    self.pause = False
        elif self.playing:
            if Input.KeyPress(PAUSE_KEY):
                Input.RemovePress(PAUSE_KEY)
                self.pause = True
            self.player.on_update(dt)
            for hurdle in self.hurdles: hurdle.on_update(dt)
            self.cur_obstacle_time -= dt
            if self.cur_obstacle_time <= 0:
                if self.rin_present:
                    self.hurdles.append(Hurdle(self, self.obstacle_x, self.ground_y))
                elif not self.rin_present:
                    self.hurdles.append(Rin(self, self.obstacle_x, self.ground_y))
                    self.rin_present = True
                self.new_obstacle_time()
    
class LoseMode(Mode):
    def __init__(self, owner):
        self.owner = owner
        self.button_x = 50
        self.button_y_offset = 150
        self.button_spacing_y = 50
        self.background = pyglet.sprite.Sprite(IMAGE["gameover1"])
        self.make_buttons()
    def make_buttons(self):
        self.buttons = []
        def _lambda():
            self.owner.set_mode_game()
        self.buttons.append(Button(self,
                                   self.button_x,
                                   SCREEN_HEIGHT - self.button_y_offset - (self.button_spacing_y * 0),
                                   "retrybutton1",
                                   _lambda))
        self.buttons.append(Button(self,
                                   self.button_x,
                                   SCREEN_HEIGHT - self.button_y_offset - (self.button_spacing_y * 1),
                                   "quitbutton1",
                                   lambda : (pyglet.app.exit(), quit())))
    def on_draw(self):
        self.background.draw()
        for button in self.buttons: button.draw()
    def on_update(self, dt):
        mouse_pos = Input.MouseDown(pyglet.window.mouse.LEFT)
        if mouse_pos:
            x, y = mouse_pos[0], mouse_pos[1]
            for button in self.buttons:
                if button.x < x < button.x + button.width:
                    if button.y < y < button.y + button.height:
                        button.on_press()

class GameRoot(pyglet.window.Window):
    def __init__(self):
        super(GameRoot, self).__init__(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.set_caption(TITLE)
        self.music_player = pyglet.media.Player()
        self.ambience_player = pyglet.media.Player()
        self.modes = {"menu" : MenuMode(self),
                      "game" : GameMode(self),
                      "lose" : LoseMode(self)}
        self.set_mode_menu()
        pyglet.clock.schedule_interval(lambda dt : self.on_update(dt), 0.01)
    def set_mode_menu(self):
        self.mode = self.modes["menu"]
##        self.music_player.queue(SOUND["focus"]) 
##        self.music_player.eos_action = "loop"
##        self.music_player.play()
    def set_mode_game(self):
        self.modes["game"] = GameMode(self)
        self.mode = self.modes["game"]
##        self.ambience_player.queue(SOUND["footsteps1"])
##        self.ambience_player.eos_action = "loop"
##        self.ambience_player.play()
    def set_mode_lose(self):
        self.mode = self.modes["lose"]
    def on_draw(self):
        self.clear()
        self.mode.on_draw()
    def on_key_press(self, symbol, modifiers):
        def _lambda(dt):
            if symbol in Input.key_press:
                Input.key_press.remove(symbol)
        Input.key_down.add(symbol)
        Input.key_press.add(symbol)
        pyglet.clock.schedule_once(_lambda, KEY_PRESS_TIME)
    def on_key_release(self, symbol, modifiers):
        Input.key_down.remove(symbol)
    def on_mouse_press(self, x, y, button, modifiers):
        Input.mouse_x = x
        Input.mouse_y = y
        Input.mouse_down.add(button)
    def on_mouse_release(self, x, y, button, modifiers):
        Input.mouse_down.remove(button)
    def on_mouse_motion(self, x, y, dx, dy):
        Input.mouse_x = x
        Input.mouse_y = y
    def on_update(self, dt):
        self.mode.on_update(dt)

if __name__ == "__main__":
    root = GameRoot()
    pyglet.app.run()
    
